Prova PHP

Desenvolvido com linguagens
- PHP
- MySQL
- HTML 5
- CSS3
- JavaScript

Bibliotecas usadas
- jQuery e jQueryUI
- Bootstrap e anexos (aplica HTML5, CSS3 e jQuery)

1) Instalação dom aplicativo
Usuário 'user', campo senha vazio, deverá constar no banco de dados MySQL. A criação de entidades e inserção de registros na tabela será automática assim que a aplicação se conectar ao banco de dados.
Caso queria editar a conexão com o banco de dados, edite o arquivo includes/bd/conn.php

2) Uso do aplicativo
Após a instalação do aplicativo, um usuário será adicionado para uso administrativo.
Login: usuario
Senha: senha


Desenvolvedor: Guilherme Faria
Portfólio: https://guilhermefgl.wordpress.com/
Contato: guilherme_fgl@hotmail.com