<?php
session_start();
if( !isset($_SESSION['loginKey']) || !( $_SESSION['loginKey'] == "01" ) ){
	header("Location: index.php");
}

$page = "funcionarios";

include "includes/bd/conn.php";

include "includes/header.php";
include "includes/menu.php";

$page = 'funcionario';
$DB = new DBConnection;
$DB->connect();

$erro = "none";
$sucesso = "none";

if( isset($_POST['atualizar']) ){
		
	if( basename($_FILES['foto']['name']) != "" ){
		$foto = "imgs/uploads/" . basename($_FILES['foto']['name']);
		move_uploaded_file($_FILES['foto']['tmp_name'], $foto);
	}else{
		$foto = "";
	}
	
	$nome = $_POST['nome'];
	$email = $_POST['email'];
	$setor = $_POST['setor'];
	$cargo = $_POST['cargo'];
	$id = $_POST['id'];
	
	if( $nome != "" && $email != "" && $setor != "" && $cargo != ""){
		if($foto != ""){
			$DB->query("UPDATE provaphp.funcionarios set nome='$nome', email='$email', setor='$setor', cargo='$cargo', foto='$foto' WHERE email='$id';");
		}else{
			$DB->query("UPDATE provaphp.funcionarios set nome='$nome', email='$email', setor='$setor', cargo='$cargo' WHERE email='$id';");
		}
		$_GET['funcionario'] = $email;
		$sucesso = "Funcionário inserido com sucesso.";
	}else{
		$erro = "Campos nescessários não preenchido.";
	}
	
}
	

?>

<div class="row">
	<div class="col-md-12">

		<div class="row">
			<div class="col-md-12">
				<h2>Prova PHP: <span class="glyphicon glyphicon-list" aria-hidden="true"></span> Atualizar funcionário</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 ">
				<form method="post" <?php echo "action=\"?funcionario={$_GET['funcionario']}\""; ?> enctype="multipart/form-data" class="form-horizontal">
					<table class="tabletable-condensed table-responsive display form-group">
						<tbody>
							
		<?php
			
			if( isset($_GET['funcionario']) ){
				$funcionario = $_GET['funcionario'];	
				$result = $DB->query( "SELECT * FROM provaphp.funcionarios WHERE email='$funcionario';" );
				if($result){
					$row = $DB->nextRow($result);
			
		?>
							
							<tr>
								<td rowspan="7" width="450px" height="300px" valign="bottom">
									Imagem do funcionário:<br />
									<input id="input" name="foto" type="file" class="file" data-preview-file-type="text"><br />
									<small>* se não quiser alterar a foto, não selecione nenhuma imagem</small>
								</td>
							</tr>
							<tr>
								<td rowspan="6" width="25px" valign="bottom">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td>
									<label class="sr-only" for="exampleInputAmount">&nbsp;Nome: </label>
									<div class="input-group">
										<div class="input-group-addon">&nbsp;Nome: *</div>
										<input type="text" class="form-control" name="nome" size="60" <?php echo "value=\"{$row['nome']}\""; ?>>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<label class="sr-only" for="exampleInputAmount">E-mail: </label>
									<div class="input-group">
										<div class="input-group-addon">E-mail: *</div>
										<input type="text" class="form-control" name="email" size="60" <?php echo "value=\"{$row['email']}\""; ?>>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<label class="sr-only" for="exampleInputAmount">&nbsp;Setor: </label>
									<div class="input-group">
										<div class="input-group-addon">&nbsp;Setor: *</div>
										<input type="text" class="form-control" name="setor" size="60" <?php echo "value=\"{$row['setor']}\""; ?>>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<label class="sr-only" for="exampleInputAmount">Cargo: </label>
									<div class="input-group">
										<div class="input-group-addon">Cargo: *</div>
										<input type="text" class="form-control" name="cargo" size="60" <?php echo "value=\"{$row['cargo']}\""; ?>>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="input-group text-center">
										<input type="hidden" name="id" <?php echo "value=\"{$_GET['funcionario']}\""; ?>>
										<button type="submit" name="atualizar" class="btn btn-primary">
											<span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> atualizar
										</button>
										&nbsp;&nbsp;&nbsp;
										<a href="funcionario.php" class="btn btn-danger">
											<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> cancelar
										</a>
									</div>
								</td>
							</tr>
							
		<?php
			
				} else {
					$erro = "funcionário não encontrado";
				}
			} else {
				$erro = "funionário não definido";
			}
			
		?>
						</tbody>
					</table>
				</form>
			</div>
		</div>

	</div>
</div>

<?php
if( $erro != "none" ){
?>
		
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">&times;</span>
				<span class="sr-only">fechar</span>
			</button>
			<strong><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Erro</strong> - <?php echo $erro; ?>
		</div>
	</div>
</div>
		
<?php
}
?>

<?php
if( $sucesso != "none" ){
?>
		
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">&times;</span>
				<span class="sr-only">fechar</span>
			</button>
			<strong><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Sucesso</strong> - <?php echo $sucesso; ?>
		</div>
	</div>
</div>
		
<?php
}
?>

<?php

include "includes/footer.php";

?>
