
$(document).ready(function() {
	
	// funções do bootstrap
	$('[data-toggle="tooltip"]').tooltip();
	$("#input").fileinput({
		image: function(vType, vName) {
        	return (typeof vType !== "undefined") ? vType.match('image.*') : vName.match(/\.(gif|png|jpe?g)$/i);
    	}
	});
	
	//campo apenas aceita números
	$('.numberOnly').keypress(function(e) {
		var a = [];
		var k = e.which;
		for ( i = 48; i < 58; i++)
			a.push(i);
		if (!(a.indexOf(k) >= 0))
			e.preventDefault();
	});

});

// função para confirmação de exlusão de registro
function confirmaDelete( url ){
	var text = '<br /><div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span> Tem certeza que deseja proceder coma  exclusão do registro?</div>';
	bootbox.confirm(text, function(result) {
		if ( result ) {                                             
			window.location = url;                              
		}
	});
}


/*
 * abaixo funções auxiliares para alteração de registro 
 */

// página de adminsitração de motoristas
function alteraMotorista(id, matricula, nome){
	$( "#id" ).val( id );
	$( "#matricula" ).val( matricula );
	$( "#nome" ).val( nome );
}
