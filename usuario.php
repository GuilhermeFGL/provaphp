<?php
session_start();
if( !isset($_SESSION['loginKey']) || !( $_SESSION['loginKey'] == "01" ) ){
	header("Location: index.php");
}

$page = "usuarios";

include "includes/bd/conn.php";

include "includes/header.php";
include "includes/menu.php";

$page = 'funcionario';
$DB = new DBConnection;
$DB->connect();

if( isset($_GET['excluir']) ){
	$DB->query("DELETE FROM provaphp.usuario WHERE login='{$_GET['excluir']}'");
}

if( isset($_POST['inserir']) ){
	$login = $_POST['login'];
	$senha = md5( $_POST['senha'] );
	$DB->query("INSERT INTO provaphp.usuario VALUES ('$login', '$senha');");
}

$erro = "none";	

?>

<div class="row">
	<div class="col-md-12">

		<div class="row">
			<div class="col-md-12">
				<h2>Prova PHP: <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Usuários</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-hover table-condensed table-responsive display">
					<thead>
						<form action="" method="post" class="form-horizontal">
							<tr>
								<th>
									<input type="text" class="form-control" name="login" placeholder="login" size="60">
								</th>
								<th>
									<input type="text" class="form-control" name="senha" placeholder="senha" size="60">
								</th>
								<th>
									<button type="submit" name="inserir" class="btn btn-primary">
												<span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> inserir
									</button>
								</th>
							</tr>
						</form>
						<tr>
							<th colspan="2">
								login
							</th>
							<th class="text-center">
								<span class="glyphicon glyphicon-trash" aria-hidden="true" title="deletar" data-toggle="tooltip" data-placement="top"></span>
							</th>
						</tr>
					</thead>
					<tbody>
						
						<?php
						
						$result = $DB->query( "SELECT * FROM provaphp.usuario ORDER BY login;" );
												
						while($row = $DB->nextRow($result)){
							echo "<tr><td colspan=\"2\">";
							echo $row['login'];
							echo "</td>";
							echo "<td class=\"text-center\"><a href=\"javascript: confirmaDelete( '?excluir={$row['login']}' );\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\" title=\"deletar\" data-toggle=\"tooltip\" data-placement=\"top\"></span></a></td>";					
							echo "</tr>";
						}
						
						?>
						
					</tbody>
				</table>
			</div>
		</div>
		
		
		<?php
		if( $erro != "none" ){
		?>
				
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">fechar</span>
					</button>
					<strong><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Erro</strong> - <?php echo $erro; ?>
				</div>
			</div>
		</div>
				
		<?php
		}
		?>

	</div>
</div>

<?php

include "includes/footer.php";

?>