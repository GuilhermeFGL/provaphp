<?php
session_start();
if( !isset($_SESSION['loginKey']) || !( $_SESSION['loginKey'] == "01" ) ){
	header("Location: index.php");
}

$page = "funcionarios";

include "includes/bd/conn.php";

include "includes/header.php";
include "includes/menu.php";

$page = 'funcionario';
$DB = new DBConnection;
$DB->connect();

$erro = "none";
$sucesso = "none";

if( isset($_POST['inserir']) ){
		
	if( basename($_FILES['foto']['name']) != "" ){
		$foto = "imgs/uploads/" . basename($_FILES['foto']['name']);
		move_uploaded_file($_FILES['foto']['tmp_name'], $foto);
	}else{
		$foto = "";
	}
	
	$nome = $_POST['nome'];
	$email = $_POST['email'];
	$setor = $_POST['setor'];
	$cargo = $_POST['cargo'];
	
	if( $nome != "" && $email != "" && $setor != "" && $cargo != ""){
		$DB->query("INSERT INTO provaphp.funcionarios VALUES ('$nome', '$email', '$setor', '$cargo', '$foto');");
		$sucesso = "Funcionário inserido com sucesso.";
	}else{
		$erro = "Campos nescessários não preenchido.";
	}
	
}
	

?>

<div class="row">
	<div class="col-md-12">
		<h2>Prova PHP: <span class="glyphicon glyphicon-list" aria-hidden="true"></span> Inserir novo funcionário</h2>
	</div>
</div>

<div class="row">
	<div class="col-md-12">

		<div class="row">
			<div class="col-md-12 ">
				<form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
					<table class="tabletable-condensed table-responsive display form-group">
						<tbody>
							<tr>
								<td rowspan="7" width="450px" height="300px" valign="bottom">
									Imagem do funcionário:<br />
									<input id="input" name="foto" type="file" class="file" data-preview-file-type="text">
								</td>
							</tr>
							<tr>
								<td rowspan="6" width="25px" valign="bottom">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td>
									<label class="sr-only" for="exampleInputAmount">&nbsp;Nome: </label>
									<div class="input-group">
										<div class="input-group-addon">&nbsp;Nome: *</div>
										<input type="text" class="form-control" name="nome" size="60">
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<label class="sr-only" for="exampleInputAmount">E-mail: </label>
									<div class="input-group">
										<div class="input-group-addon">E-mail: *</div>
										<input type="text" class="form-control" name="email" size="60">
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<label class="sr-only" for="exampleInputAmount">&nbsp;Setor: </label>
									<div class="input-group">
										<div class="input-group-addon">&nbsp;Setor: *</div>
										<input type="text" class="form-control" name="setor" size="60">
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<label class="sr-only" for="exampleInputAmount">Cargo: </label>
									<div class="input-group">
										<div class="input-group-addon">Cargo: *</div>
										<input type="text" class="form-control" name="cargo" size="60">
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="input-group text-center">
										<button type="submit" name="inserir" class="btn btn-primary">
											<span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> inserir
										</button>
										&nbsp;&nbsp;&nbsp;
										<a href="funcionario.php" class="btn btn-danger">
											<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> cancelar
										</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>

	</div>
</div>

<?php
if( $erro != "none" ){
?>
		
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">&times;</span>
				<span class="sr-only">fechar</span>
			</button>
			<strong><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Erro</strong> - <?php echo $erro; ?>
		</div>
	</div>
</div>
		
<?php
}
?>

<?php
if( $sucesso != "none" ){
?>
		
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">&times;</span>
				<span class="sr-only">fechar</span>
			</button>
			<strong><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Sucesso</strong> - <?php echo $sucesso; ?>
		</div>
	</div>
</div>
		
<?php
}
		?>


<?php

include "includes/footer.php";

?>
