<?php
session_start();

if( !isset($_SESSION['loginKey']) || !( $_SESSION['loginKey'] == "01" ) ){
	header("Location: index.php");
}

$page = 'main';

include "includes/bd/conn.php";

include "includes/header.php";
include "includes/menu.php";

?>

	<article class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="jumbotron">
				<h2>Escolha uma das opções abaixo</h2>
			  	<p>
			  		Desenvolvido por Guilherme Faria - <a href="https://guilhermefgl.wordpress.com/" title="portfólio">Portfólio On-line</a>
			  	</p>
			  	<p>
			  		<a class="btn btn-primary btn" href="funcionario.php" role="button"><span class="glyphicon glyphicon-list" aria-hidden="true"> Funcionários</a><br /><br />
			  		<a class="btn btn-primary btn" href="usuario.php" role="button"><span class="glyphicon glyphicon-user" aria-hidden="true"> Usuários</a><br /><br />
			  		<a class="btn btn-primary btn" href="https://bitbucket.org/GuilhermeFGL/provaphp" role="button"><span class="glyphicon glyphicon-cloud" aria-hidden="true"> Repositório Git</a>
			  	</p>
			</div>
		</div>
	</article>
	

<?php

include "includes/footer.php";

?>