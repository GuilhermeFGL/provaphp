<?php
session_start();

if( isset($_GET['logout']) && $_GET['logout'] == "1" ){
	session_destroy();
}

include "includes/bd/conn.php";
$DB = new DBConnection;
$DB->connect();
$DB->createDB();

if(isset($_POST['login'])){
	$login = $_POST['login'];
	$senha = $_POST['pass'];
	if($DB->validaLogin($login, $senha)){
		$_SESSION['loginKey'] = "01";	
	}
}

if( isset($_SESSION['loginKey']) && $_SESSION['loginKey'] == "01" ){
	header("Location: main.php");
}

include "includes/header.php";

?>
		
		<div class="row">
			<div class="col-md-12">
				<p>
					&nbsp;
				</p>
			</div>
		</div>
		
		<div class="row">
			<form class="col-md-6 col-md-offset-3" method="post" action="">
				
<?php
if( isset($_POST['login']) && $_POST['login'] == "" ){
	echo '<div class="input-group has-error">';
}else{
	echo '<div class="input-group">';
}
?>
					<span class="input-group-addon glyphicon glyphicon-user" aria-hidden="true"></span>
					<input type="text" class="form-control input-lg" placeholder="Login" name="login"  style="margin-top: 1px !important; margin-bottom: -1px !important;" autofocus>
				</div>
				<p>
					&nbsp;
				</p>
				
<?php
if( isset($_POST['pass']) && $_POST['pass'] == "" ){
	echo '<div class="input-group has-error">';
}else{
	echo '<div class="input-group">';
}
?>
				
					<span class="input-group-addon glyphicon glyphicon-lock" aria-hidden="true"></span>
					<input type="password" class="form-control input-lg" placeholder="Senha" name="pass" style="margin-top: 1px !important; margin-bottom: -1px !important;">
				</div>
				<p>
					&nbsp;
				</p>
				<div class="form-group">
					<button class="btn btn-primary btn-lg btn-block">Entrar <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span></button>
				</div>
			</form>
		</div>
		
<?php
if( isset($_POST['login']) ){
?>
		
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">fechar</span>
					</button>
					<strong>Erro</strong> - login inválido
				</div>
			</div>
		</div>
		
<?php
}

include "includes/footer.php";

?>