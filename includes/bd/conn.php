<?php
	
	class DBConnection{
		
		private $mysqli;
		
		public function __construct() {
			mb_internal_encoding("UTF-8");  
			ob_start("mb_output_handler");   
			header('Content-Type: text/html; charset=utf-8');
		}
		
		/*
		 * conecta ao banco de dados
		 */
		 public function connect(){
		 	$this->mysqli = new mysqli('localhost:3306', 'user','') or die (mysqli_connect_errno());
			$this->mysqli->query('SET CHARACTER SET utf8');
		 }
			 
		/*
		 * crea data base e tabelas padrões do projeto, insere dados para funcionários e login
		 */
		public function createDB(){
		 	$this->mysqli->query("CREATE DATABASE IF NOT EXISTS provaPHP;");
			$this->mysqli->query("USE provaphp;");
		 	
		 	$this->mysqli->query("CREATE TABLE IF NOT EXISTS funcionarios ( 
		 		nome VARCHAR(50) NOT NULL, 
		 		email VARCHAR(30) NOT NULL, 
		 		setor VARCHAR(30),
		 		cargo VARCHAR(30),
		 		foto VARCHAR(30),
		 		PRIMARY KEY (email) );"
			);
			$this->mysqli->query("CREATE TABLE IF NOT EXISTS usuario ( 
				login VARCHAR(10) NOT NULL, 
				senha VARCHAR(32) NOT NULL, 
				PRIMARY KEY (login) );"
			);
			
			$result = $this->mysqli->query("SELECT email FROM funcionarios;");
			if( $result->num_rows == 0){	
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('Pedro','pedro@dj.emp.br','TI','Gerente','');" );
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('João','joao@dj.emp.br','TI','Programador','');" );
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('Flavio','flavio@dj.emp.br','TI','Estagiário','');" );
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('Maria','maria@dj.emp.br','Administrativo','Secretária','');" );
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('Amanda','amanda@dj.emp.br','Administrativo','Estagiária','');" );
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('Manoel','manoel@dj.emp.br','Financeiro','Contador','');" );
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('Laura','laura@dj.emp.br','RH','Psicóloga','');" );
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('Debora','debora@dj.emp.br','RH','Gerente','');" );
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('Bruno','bruno@dj.emp.br','Diretoria','CEO','');" );
				$this->mysqli->query( "INSERT INTO funcionarios VALUES ('Rodrigo','rodrigo@dj.emp.br','Diretoria','CFO','');" );
			}
			
			$result = $this->mysqli->query("SELECT login FROM usuario;");
			if( $result->num_rows == 0){
				$this->mysqli->query( "INSERT INTO usuario VALUES ('usuario','" . md5("senha") . "');" );
			}
		}
			 
		 /*
		  * valida login do usuario
		  */
		public function validaLogin($usuario, $senha){
		  	$senha = md5($senha);
	  		$result = $this->mysqli->query("SELECT login FROM usuario WHERE login='$usuario' AND senha='$senha' LIMIT 1;");
			if( $result->num_rows > 0 ){
				return true;	
			}else{
				return false;
			}
		}
		  
		/*
		 * executa query e salva resultado em variável 
		 */
	   	public function query($query){
			return $this->mysqli->query($query);	
		}
		
		/*
		 * retorna proxima linha da pesquisa 
		 */
		public function nextRow($result){
			return $result->fetch_assoc();
		}
		
	}

?>
