<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" style="cursor: auto !important;">
				<img alt="logo" src="imgs/logo.jpg" width="25px">
			</a>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<?php echo ($page == 'main')? "<li class='active'>": "<li>"; ?>
					<a href="main.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Painel</a>	
				</li>
				<?php echo ($page == 'funcionarios')? "<li class='active'>": "<li>"; ?>
					<a href="funcionario.php"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Funcionários</a>	
				</li>
				<?php echo ($page == 'usuarios')? "<li class='active'>": "<li>"; ?>
					<a href="usuario.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Usuários</a>	
				</li>
				<li>
					<a href="https://bitbucket.org/GuilhermeFGL/provaphp"><span class="glyphicon glyphicon-cloud" aria-hidden="true"></span> Repositório Git</a>	
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a class="alert-danger" href="index.php?logout=1">Logout <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a></li>
			</ul>
		</div>
	</div>
</nav>
