<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">
	
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Prova PHP</title>
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" />
	<link rel="stylesheet" type="text/css" href="css/fileinput.min.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	
</head>

<body>

	<section id="wrap" class="container">
		
		<header class="page-header">
			<h1>
				Prova PHP<br />
			</h1>
		</header>
		