<?php
session_start();
if( !isset($_SESSION['loginKey']) || !( $_SESSION['loginKey'] == "01" ) ){
	header("Location: index.php");
}

$page = "funcionarios";

include "includes/bd/conn.php";

include "includes/header.php";
include "includes/menu.php";

$page = 'funcionario';
$DB = new DBConnection;
$DB->connect();

if( isset($_GET['excluir']) ){
	$DB->query("DELETE FROM provaphp.funcionarios WHERE email='{$_GET['excluir']}'");
}

$erro = "none";	

?>

<div class="row">
	<div class="col-md-12">

		<div class="row">
			<div class="col-md-12">
				<h2>Prova PHP: <span class="glyphicon glyphicon-list" aria-hidden="true"></span> Funcionarios</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<p>
					<br />
					<a href="inserir.php" title="adicionar novo funcionário">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Adicionar novo funcionário
					</a>
				</p>
			</div>
			<div class="col-md-8 text-right">
				<p>		
					<form method="post" action="" class="form-inline">
						<div class="form-group">
							<label class="sr-only" for="exampleInputAmount">pesquisar: </label>
							<div class="input-group">
								<div class="input-group-addon">pesquisar: </div>
								<input type="text" class="form-control" name="pesquisa">
							</div>
							<label class="sr-only" for="exampleInputAmount">por: </label>
							<div class="input-group">
								<div class="input-group-addon">por: </div>
								<select class="form-control"name="tipo">
									<option value="">todas colunas</option>
									<option value="nome">nome</option>
									<option value="email">e-mail</option>
									<option value="setor">setor</option>
									<option value="cargo">cargo</option>
								</select>
							</div>
							<div class="input-group">
								<button type="submit" name="pesquisar" class="btn btn-primary btn-group-justified ">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span> pesquisar
								</button>
							</div>
						</div>
					</form>
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-hover table-condensed table-responsive display">
					<thead>
						<tr>
							<th>
								nome
							</th>
							<th>
								e-mail
							</th>
							<th>
								setor
							</th>
							<th>
								cargo
							</th>
							<th class="text-center">
								<span class="glyphicon glyphicon-eye-open" aria-hidden="true" title="visualizar" data-toggle="tooltip" data-placement="top"></span>
							</th>
							<th class="text-center">
								<span class="glyphicon glyphicon-edit" aria-hidden="true" title="editar" data-toggle="tooltip" data-placement="top"></span>
							</th>
							<th class="text-center">
								<span class="glyphicon glyphicon-trash" aria-hidden="true" title="deletar" data-toggle="tooltip" data-placement="top"></span>
							</th>
						</tr>
					</thead>
					<tbody>
						
						<?php
						
						if( isset($_POST['pesquisar']) ){
							$pesquisa = $_POST['pesquisa'];
							$tipo = $_POST['tipo'];
							if( $tipo != ""){
								$result = $DB->query( "SELECT * FROM provaphp.funcionarios WHERE $tipo LIKE '$pesquisa' ORDER BY nome;" );
							}else{
								$result = $DB->query( "SELECT * FROM provaphp.funcionarios WHERE nome LIKE '$pesquisa' OR email LIKE '$pesquisa' OR cargo LIKE '$pesquisa' OR setor LIKE '$pesquisa' ORDER BY nome;" );
							}
							echo "<tr><td colspan=\"7\">Pesquisando por: $pesquisa. <a href=\"\">Voltar para tabela completa</a>.</td></tr>";
						}else{
							$result = $DB->query( "SELECT * FROM provaphp.funcionarios ORDER BY nome;" );
						}
						
						while($row = $DB->nextRow($result)){
							echo "<tr><td>";
							echo $row['nome'];
							echo "</td><td>";
							echo $row['email'];
							echo "</td><td>";
							echo $row['setor'];
							echo "</td><td>";
							echo $row['cargo'];
							echo "<td class=\"text-center\"><a href=\"visualiza.php?funcionario={$row['email']}\"><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\" title=\"visualizar\" data-toggle=\"tooltip\" data-placement=\"top\"></span></a></td>";
							echo "<td class=\"text-center\"><a href=\"editar.php?funcionario={$row['email']}\"><span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\" title=\"editar\" data-toggle=\"tooltip\" data-placement=\"top\"></span></a></td>";
							echo "<td class=\"text-center\"><a href=\"javascript: confirmaDelete( '?excluir={$row['email']}' );\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\" title=\"deletar\" data-toggle=\"tooltip\" data-placement=\"top\"></span></a></td>";
							
							"";
							
														
							echo "</td></tr>";
						}
						
						?>
						
					</tbody>
				</table>
			</div>
		</div>
		
		
		<?php
		if( $erro != "none" ){
		?>
				
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">fechar</span>
					</button>
					<strong><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Erro</strong> - <?php echo $erro; ?>
				</div>
			</div>
		</div>
				
		<?php
		}
		?>

	</div>
</div>

<?php

include "includes/footer.php";

?>