<?php
session_start();
if( !isset($_SESSION['loginKey']) || !( $_SESSION['loginKey'] == "01" ) ){
	header("Location: index.php");
}

$page = "funcionarios";

include "includes/bd/conn.php";

include "includes/header.php";
include "includes/menu.php";

$page = 'funcionario';
$DB = new DBConnection;
$DB->connect();

$erro = "none";	

?>

<div class="row">
	<div class="col-md-12">

		<div class="row">
			<div class="col-md-12">
				<h2>Prova PHP: <span class="glyphicon glyphicon-list" aria-hidden="true"></span> Funcionarios</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table class="tabletable-condensed table-responsive display">
					<tbody>
						
						<?php
					
						$funcionario = $_GET['funcionario'];	
						$result = $DB->query( "SELECT * FROM provaphp.funcionarios WHERE email='$funcionario';" );
						if($result){
							$row = $DB->nextRow($result);
							
							echo "<tr><td rowspan=\"6\">";
							if( $row['foto'] == "" ){
								echo "<img class=\"thumbnail profile\" src=\"imgs/profile.png\" alt=\"profile\" width=\"250px\" />";
							}else{
								echo "<img class=\"thumbnail profile\" src=\"{$row['foto']}\" alt=\"profile\" width=\"250px\" />";
							}
							
							echo "</td></tr><tr><td colspan=\"3\"><h2>&nbsp;&nbsp;&nbsp;";
							echo $row['nome'];
							echo "</h2></td></tr><tr><td colspan=\"3\">&nbsp;&nbsp;&nbsp;<strong>e-mail:</strong> ";
							echo $row['email'];
							echo "</td></tr><tr><td colspan=\"3\">&nbsp;&nbsp;&nbsp;<strong>setor:</strong> ";
							echo $row['setor'];
							echo "</td></tr><tr><td colspan=\"3\">&nbsp;&nbsp;&nbsp;<strong>cargo:</strong> ";
							echo $row['cargo'];
							echo "</td></tr>";
							echo "<tr><td>&nbsp;&nbsp;&nbsp;<a href=\"edita.php?funcionario={$row['email']}\"><span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\" title=\"editar\" data-toggle=\"tooltip\" data-placement=\"top\"></span> editar funcionário</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
							echo "<td><a href=\"funcionario.php?excluir={$row['email']}\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\" title=\"deletar\" data-toggle=\"tooltip\" data-placement=\"top\"></span> deletar funcionário</a></td>";
							echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"funcionario.php\"><span class=\"glyphicon glyphicon-arrow-left\" aria-hidden=\"true\" title=\"voltar\" data-toggle=\"tooltip\" data-placement=\"top\"></span> voltar à funcionários</a></td></tr>";

						}else{
							$erro = "Funcionário não encontrado";
						}

						
						?>
						
					</tbody>
				</table>
			</div>
		</div>
		
		
		<?php
		if( $erro != "none" ){
		?>
				
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">fechar</span>
					</button>
					<strong><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Erro</strong> - <?php echo $erro; ?>
				</div>
			</div>
		</div>
				
		<?php
		}
		?>

	</div>
</div>

<?php

include "includes/footer.php";

?>